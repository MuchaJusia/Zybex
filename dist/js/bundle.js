/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/Main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./src/scss/style.scss":
/*!************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js!./src/scss/style.scss ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "* {\n  margin: 0px;\n  padding: 0px;\n  box-sizing: border-box;\n  overflow: hidden; }\n\n#commodore {\n  height: 850px;\n  width: 900px;\n  display: block;\n  margin: 0px auto;\n  padding: 15px;\n  background-color: #A39994; }\n  #commodore #firstBorder {\n    width: 870px;\n    height: 770px;\n    background-color: #66656B;\n    display: block;\n    padding: 15px;\n    border-radius: 10px; }\n    #commodore #firstBorder #secondBorder {\n      width: 840px;\n      height: 740px;\n      background-color: #3D3B3E;\n      display: block;\n      padding: 20px;\n      border-radius: 10px; }\n      #commodore #firstBorder #secondBorder #screen {\n        width: 800px;\n        height: 700px;\n        background-color: #858F8F;\n        border-radius: 10px; }\n        #commodore #firstBorder #secondBorder #screen #canvas {\n          width: 800px;\n          height: 700px;\n          border-radius: 10px; }\n  #commodore #logo {\n    display: block;\n    margin: 0 auto;\n    height: 65px; }\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/css-base.js":
/*!*************************************************!*\
  !*** ./node_modules/css-loader/lib/css-base.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target) {
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertInto + " " + options.insertAt.before);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/js/Canvas.js":
/*!**************************!*\
  !*** ./src/js/Canvas.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Canvas;
function Canvas(image, settings, player) {
    var _this = this;

    var canvas = document.getElementById("canvas");
    var context = canvas.getContext("2d");

    this.playfieldX = 0;
    this.run = 1;

    this.clearCanvas = function () {
        context.clearRect(0, 0, 800, 700);
        context.fillStyle = 'black';
        context.fillRect(0, 0, 800, 700);
    };

    this.drawPlayfield = function () {
        context.save();
        context.drawImage(image.playfield, _this.playfieldX, 0, settings.playfieldWidth, 700);
        context.restore();

        _this.playfieldX -= settings.playerSpeed;

        _this.playfieldX <= -(24500 - 800) ? _this.run++ : null;
        _this.playfieldX <= -(24500 - 800) ? player.score += 1000 : null;
        _this.playfieldX <= -(24500 - 800) ? _this.playfieldX = 0 : null;
    };

    this.drawPlayer = function () {
        context.save();
        context.drawImage(image[player.image], player.x, player.y);
        context.restore();
    };

    this.drawPlayerBullets = function (bulletsArray) {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = bulletsArray[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var bullet = _step.value;

                context.save();
                context.drawImage(image.bullet, bullet.x, bullet.y);
                context.restore();
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }
    };

    this.drawEnemiesBullets = function (bulletsArray) {
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
            for (var _iterator2 = bulletsArray[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var bullet = _step2.value;

                context.save();
                context.drawImage(image.enemyBullet, bullet.x, bullet.y);
                context.restore();
            }
        } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                    _iterator2.return();
                }
            } finally {
                if (_didIteratorError2) {
                    throw _iteratorError2;
                }
            }
        }
    };

    this.drawScreen = function (screen) {
        if (screen == "END") {
            context.save();
            context.drawImage(image.theEnd, 0, 0);
            context.restore();
        } else if (screen == "OVER") {
            context.drawImage(image.gameOver, 0, 0);
        }
    };

    this.drawScore = function () {
        context.save();
        context.font = "15px Lucida Console";
        context.fillStyle = "white";
        context.fillText("Score: " + player.score + " \u2764: " + player.lives, 10, 20);
        context.restore();
    };
}

/***/ }),

/***/ "./src/js/Collision.js":
/*!*****************************!*\
  !*** ./src/js/Collision.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Collision;
function Collision(canvas, settings, player) {
    var _this = this;

    this.playfieldArray = [{ name: "ground", from: { x: 0, y: 685 }, to: { x: 24500, y: 700 } }, { name: "flower", from: { x: 1073, y: 607 }, to: { x: 1165, y: 700 } }, { name: "flower", from: { x: 1285, y: 607 }, to: { x: 1377, y: 700 } }, { name: "flower", from: { x: 1607, y: 607 }, to: { x: 1700, y: 700 } }, { name: "flower", from: { x: 1707, y: 413 }, to: { x: 1812, y: 700 } }, { name: "flower", from: { x: 1812, y: 507 }, to: { x: 1916, y: 700 } }, { name: "mushroom", from: { x: 2120, y: 600 }, to: { x: 2265, y: 700 } }, { name: "mushroom", from: { x: 2870, y: 600 }, to: { x: 3000, y: 700 } }, { name: "flower", from: { x: 4706, y: 607 }, to: { x: 4800, y: 700 } }, { name: "pyramid", from: { x: 4808, y: 670 }, to: { x: 5080, y: 700 } }, { name: "pyramid", from: { x: 4841, y: 644 }, to: { x: 5050, y: 670 } }, { name: "pyramid", from: { x: 4870, y: 624 }, to: { x: 5026, y: 644 } }, { name: "flower", from: { x: 4895, y: 511 }, to: { x: 5000, y: 624 } }, { name: "highground", from: { x: 5155, y: 660 }, to: { x: 5235, y: 700 } }, { name: "highground", from: { x: 5176, y: 636 }, to: { x: 5235, y: 700 } }, { name: "highground", from: { x: 5208, y: 607 }, to: { x: 5235, y: 700 } }, { name: "highground", from: { x: 5235, y: 581 }, to: { x: 7878, y: 700 } }, { name: "bar", from: { x: 5641, y: 525 }, to: { x: 5679, y: 700 } }, { name: "bar", from: { x: 5850, y: 525 }, to: { x: 5891, y: 700 } }, { name: "bar", from: { x: 5953, y: 525 }, to: { x: 5993, y: 700 } }, { name: "mushroom", from: { x: 6005, y: 500 }, to: { x: 6177, y: 700 } }, { name: "bar", from: { x: 6874, y: 525 }, to: { x: 6911, y: 700 } }, { name: "bar", from: { x: 7074, y: 525 }, to: { x: 7118, y: 700 } }, { name: "bar", from: { x: 7181, y: 525 }, to: { x: 7220, y: 700 } }, { name: "mushroom", from: { x: 7231, y: 500 }, to: { x: 7402, y: 700 } }, { name: "highground", from: { x: 7878, y: 597 }, to: { x: 7901, y: 700 } }, { name: "highground", from: { x: 7901, y: 623 }, to: { x: 7928, y: 700 } }, { name: "highground", from: { x: 7928, y: 6480 }, to: { x: 7954, y: 700 } }, { name: "flower", from: { x: 9913, y: 502 }, to: { x: 10016, y: 624 } }, { name: "mushroom", from: { x: 10059, y: 600 }, to: { x: 10208, y: 700 } }, { name: "mushroom", from: { x: 10266, y: 600 }, to: { x: 10419, y: 700 } }, { name: "mushroom", from: { x: 12250, y: 600 }, to: { x: 12404, y: 700 } }, { name: "flower", from: { x: 12916, y: 600 }, to: { x: 13007, y: 700 } }, { name: "flower", from: { x: 13126, y: 600 }, to: { x: 13216, y: 700 } }, { name: "flower", from: { x: 13449, y: 600 }, to: { x: 13542, y: 700 } }, { name: "flower", from: { x: 13449, y: 600 }, to: { x: 13653, y: 700 } }, { name: "flower", from: { x: 13653, y: 509 }, to: { x: 13762, y: 700 } }, { name: "flower", from: { x: 14304, y: 509 }, to: { x: 14403, y: 700 } }, { name: "mushroom", from: { x: 14649, y: 600 }, to: { x: 14805, y: 700 } }, { name: "mushroom", from: { x: 14922, y: 600 }, to: { x: 15076, y: 700 } }, { name: "flower", from: { x: 15375, y: 607 }, to: { x: 15469, y: 700 } }, { name: "flower", from: { x: 15588, y: 607 }, to: { x: 15680, y: 700 } }, { name: "flower", from: { x: 15914, y: 607 }, to: { x: 16005, y: 700 } }, { name: "flower", from: { x: 16012, y: 418 }, to: { x: 16115, y: 700 } }, { name: "flower", from: { x: 16115, y: 511 }, to: { x: 16227, y: 700 } }, { name: "pyramid", from: { x: 18942, y: 646 }, to: { x: 19260, y: 700 } }, { name: "pyramid", from: { x: 18976, y: 594 }, to: { x: 19225, y: 700 } }, { name: "pyramid", from: { x: 19030, y: 540 }, to: { x: 19172, y: 700 } }, { name: "pyramid", from: { x: 19073, y: 512 }, to: { x: 19129, y: 700 } }, { name: "pyramid", from: { x: 18942 + 960, y: 646 }, to: { x: 19260 + 960, y: 700 } }, { name: "pyramid", from: { x: 18976 + 960, y: 594 }, to: { x: 19225 + 960, y: 700 } }, { name: "pyramid", from: { x: 19030 + 960, y: 540 }, to: { x: 19172 + 960, y: 700 } }, { name: "pyramid", from: { x: 19073 + 960, y: 512 }, to: { x: 19129 + 960, y: 700 } }, { name: "pyramid", from: { x: 18942 + 2139, y: 646 }, to: { x: 19260 + 2139, y: 700 } }, { name: "pyramid", from: { x: 18976 + 2139, y: 594 }, to: { x: 19225 + 2139, y: 700 } }, { name: "pyramid", from: { x: 19030 + 2139, y: 540 }, to: { x: 19172 + 2139, y: 700 } }, { name: "pyramid", from: { x: 19073 + 2139, y: 512 }, to: { x: 19129 + 2139, y: 700 } }, { name: "pyramid", from: { x: 18942 + 2676, y: 646 }, to: { x: 19260 + 2676, y: 700 } }, { name: "pyramid", from: { x: 18976 + 2676, y: 594 }, to: { x: 19225 + 2676, y: 700 } }, { name: "pyramid", from: { x: 19030 + 2676, y: 540 }, to: { x: 19172 + 2676, y: 700 } }, { name: "pyramid", from: { x: 19073 + 2676, y: 512 }, to: { x: 19129 + 2676, y: 700 } }, { name: "pyramid", from: { x: 18942 + 3209, y: 646 }, to: { x: 19260 + 3209, y: 700 } }, { name: "pyramid", from: { x: 18976 + 3209, y: 594 }, to: { x: 19225 + 3209, y: 700 } }, { name: "pyramid", from: { x: 19030 + 3209, y: 540 }, to: { x: 19172 + 3209, y: 700 } }, { name: "pyramid", from: { x: 19073 + 3209, y: 512 }, to: { x: 19129 + 3209, y: 700 } }, { name: "pyramid", from: { x: 18942 + 3742, y: 646 }, to: { x: 19260 + 3742, y: 700 } }, { name: "pyramid", from: { x: 18976 + 3742, y: 594 }, to: { x: 19225 + 3742, y: 700 } }, { name: "pyramid", from: { x: 19030 + 3742, y: 540 }, to: { x: 19172 + 3742, y: 700 } }, { name: "pyramid", from: { x: 19073 + 3742, y: 512 }, to: { x: 19129 + 3742, y: 700 } }];

    this.checkCollision = function (player) {
        if (!player.invisible) {
            var playerX = player.x + player.width - canvas.playfieldX;
            var playerY = player.y + player.height;
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = _this.playfieldArray[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var collision = _step.value;

                    if (playerX >= collision.from.x && playerX <= collision.to.x && playerY >= collision.from.y && playerY <= collision.to.y) {
                        player.dead();
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
    };

    this.checkPlayerBulletsCollision = function (enemiesArray, playerBullets) {
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
            for (var _iterator2 = playerBullets[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var bullet = _step2.value;

                var bulletX = bullet.x + bullet.width;
                var bulletY = bullet.y + bullet.height / 2;
                var _iteratorNormalCompletion3 = true;
                var _didIteratorError3 = false;
                var _iteratorError3 = undefined;

                try {
                    for (var _iterator3 = enemiesArray[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                        var enemy = _step3.value;

                        if (enemy.invisible == false) {
                            if (bulletX >= enemy.x && bulletX <= enemy.x + enemy.width && bulletY >= enemy.y && bulletY <= enemy.y + enemy.height) {
                                bullet.deleteBullet();
                                enemy.lives--;
                                if (enemy.lives <= 0) {
                                    enemy.delete();
                                    player.score += 100;
                                    settings.bulletsTime >= 150 ? settings.bulletsTime -= 10 : null;
                                }
                            }
                        }
                    }
                } catch (err) {
                    _didIteratorError3 = true;
                    _iteratorError3 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion3 && _iterator3.return) {
                            _iterator3.return();
                        }
                    } finally {
                        if (_didIteratorError3) {
                            throw _iteratorError3;
                        }
                    }
                }
            }
        } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                    _iterator2.return();
                }
            } finally {
                if (_didIteratorError2) {
                    throw _iteratorError2;
                }
            }
        }
    };

    this.checkEnemiesBulletsCollision = function (player, enemiesBullets) {
        var _iteratorNormalCompletion4 = true;
        var _didIteratorError4 = false;
        var _iteratorError4 = undefined;

        try {
            for (var _iterator4 = enemiesBullets[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                var bullet = _step4.value;

                var bulletX = bullet.x;
                var bulletY = bullet.y + bullet.height / 2;
                if (!player.invisible) {
                    if (bulletX >= player.x && bulletX <= player.x + player.width && bulletY >= player.y && bulletY <= player.y + player.height) {
                        bullet.deleteBullet();
                        player.dead();
                    }
                }
            }
        } catch (err) {
            _didIteratorError4 = true;
            _iteratorError4 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion4 && _iterator4.return) {
                    _iterator4.return();
                }
            } finally {
                if (_didIteratorError4) {
                    throw _iteratorError4;
                }
            }
        }
    };

    this.checkPlayerCollisionWithEnemies = function (player, enemiesArray) {
        var playerX = player.x + player.width;
        var playerTopY = player.y;
        var playerBottomY = player.y + player.height;
        var _iteratorNormalCompletion5 = true;
        var _didIteratorError5 = false;
        var _iteratorError5 = undefined;

        try {
            for (var _iterator5 = enemiesArray[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                var enemy = _step5.value;

                if (!player.invisible && !enemy.invisible) {
                    if (playerX >= enemy.x && playerX <= enemy.x + enemy.width && playerTopY >= enemy.y && playerTopY <= enemy.y + enemy.height || playerX >= enemy.x && playerX <= enemy.x + enemy.width && playerBottomY >= enemy.y && playerBottomY <= enemy.y + enemy.height) {
                        player.dead();
                    }
                }
            }
        } catch (err) {
            _didIteratorError5 = true;
            _iteratorError5 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion5 && _iterator5.return) {
                    _iterator5.return();
                }
            } finally {
                if (_didIteratorError5) {
                    throw _iteratorError5;
                }
            }
        }
    };
}

/***/ }),

/***/ "./src/js/Enemies.js":
/*!***************************!*\
  !*** ./src/js/Enemies.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Enemies;

var _NormalEnemy = __webpack_require__(/*! ./Enemies/NormalEnemy.js */ "./src/js/Enemies/NormalEnemy.js");

var _NormalEnemy2 = _interopRequireDefault(_NormalEnemy);

var _FlyEnemy = __webpack_require__(/*! ./Enemies/FlyEnemy.js */ "./src/js/Enemies/FlyEnemy.js");

var _FlyEnemy2 = _interopRequireDefault(_FlyEnemy);

var _WormEnemy = __webpack_require__(/*! ./Enemies/WormEnemy.js */ "./src/js/Enemies/WormEnemy.js");

var _WormEnemy2 = _interopRequireDefault(_WormEnemy);

var _SnakeEnemy = __webpack_require__(/*! ./Enemies/SnakeEnemy.js */ "./src/js/Enemies/SnakeEnemy.js");

var _SnakeEnemy2 = _interopRequireDefault(_SnakeEnemy);

var _Rock = __webpack_require__(/*! ./Enemies/Rock.js */ "./src/js/Enemies/Rock.js");

var _Rock2 = _interopRequireDefault(_Rock);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Enemies(images, canvas) {
    this.addNormalEnemies = function (enemiesArray, height) {
        for (var i = 0; i < 5; i++) {
            var color = null;
            if (Math.round(Math.random())) color = "red";else color = "blue";

            enemiesArray.push(new _NormalEnemy2.default(Math.random() * 200 + 800, height + 40 * i, color, images));
        }
        return enemiesArray;
    };

    this.addFlyEnemies = function (enemiesArray, height) {
        for (var i = 0; i < 8; i++) {
            enemiesArray.push(new _FlyEnemy2.default(800, height + 80 * i, images));
        }

        return enemiesArray;
    };

    this.addWormEnemies = function (enemiesArray, height) {
        for (var i = 0; i < 5; i++) {
            i == 0 ? enemiesArray.push(new _WormEnemy2.default(800, height + 60 * i, images, "wormHead", i * 100)) : enemiesArray.push(new _WormEnemy2.default(800, height + 60 * i, images, "wormBody", i * 100));
        }

        return enemiesArray;
    };

    this.addSnakeEnemies = function (enemiesArray, height) {
        for (var i = 0; i < 8; i++) {
            enemiesArray.push(new _SnakeEnemy2.default(720, height + 50 * i, images, i * 150));
        }

        return enemiesArray;
    };

    this.addRocks = function (enemiesArray) {
        for (var i = 0; i < 25; i++) {
            enemiesArray.push(new _Rock2.default(images, i * 200));
        }

        return enemiesArray;
    };
}

/***/ }),

/***/ "./src/js/Enemies/FlyEnemy.js":
/*!************************************!*\
  !*** ./src/js/Enemies/FlyEnemy.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = FlyEnemy;
function FlyEnemy(x, y, images) {
    var _this = this;

    var canvas = document.getElementById("canvas");
    var context = canvas.getContext("2d");

    this.name = "fly";
    this.lives = 2;
    this.invisible = false;
    this.x = x;
    this.y = y;
    this.width = 70;
    this.height = 70;
    this.number = 0;
    this.image = "grayFly" + this.number;
    this.speed = 1.5;

    this.update = function () {
        context.save();
        context.drawImage(images[_this.image], _this.x, _this.y);
        context.restore();
        _this.x -= _this.speed;
    };

    this.delete = function () {
        _this.invisible = true;
        clearInterval(interval);
        setTimeout(function () {
            _this.image = 'dead0';
        }, 200);
        setTimeout(function () {
            _this.image = 'dead1';
        }, 400);
        setTimeout(function () {
            _this.image = 'dead2';
        }, 600);
        setTimeout(function () {
            _this.image = 'dead3';
        }, 800);
        setTimeout(function () {
            _this.image = 'dead4';
        }, 1000);
        setTimeout(function () {
            _this.x = -1000;
        }, 1200);
    };

    setTimeout(function () {
        _this.speed = 0;
    }, 1000);

    setTimeout(function () {
        _this.speed = 14;
    }, Math.random() * 1000 + 3000);

    var direction = "up";

    var interval = setInterval(function () {
        if (direction == "up") {
            _this.number++;
        } else {
            _this.number--;
        }

        _this.image = "grayFly" + _this.number;

        _this.number == 3 ? direction = "down" : null;
        _this.number == 0 ? direction = "up" : null;
    }, 1000 / 10);
}

/***/ }),

/***/ "./src/js/Enemies/NormalEnemy.js":
/*!***************************************!*\
  !*** ./src/js/Enemies/NormalEnemy.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = NormalEnemy;
function NormalEnemy(x, y, color, images) {
    var _this = this;

    var canvas = document.getElementById("canvas");
    var context = canvas.getContext("2d");

    this.name = "normal";
    this.lives = 2;
    this.invisible = false;
    this.x = x;
    this.y = y;
    this.width = 80;
    this.height = 69;
    this.color = color;
    this.number = 0;
    this.image = this.color + "Enemy" + this.number;

    this.update = function () {
        context.save();
        context.drawImage(images[_this.image], _this.x, _this.y);
        context.restore();
        _this.x -= 1.5;
    };

    this.delete = function () {
        _this.invisible = true;
        clearInterval(interval);
        setTimeout(function () {
            _this.image = 'dead0';
        }, 200);
        setTimeout(function () {
            _this.image = 'dead1';
        }, 400);
        setTimeout(function () {
            _this.image = 'dead2';
        }, 600);
        setTimeout(function () {
            _this.image = 'dead3';
        }, 800);
        setTimeout(function () {
            _this.image = 'dead4';
        }, 1000);
        setTimeout(function () {
            _this.x = -1000;
        }, 1200);
    };

    var direction = "up";

    var interval = setInterval(function () {
        if (direction == "up") {
            _this.number++;
            _this.x -= 10;
        } else {
            _this.number--;
            _this.x += 10;
        }

        _this.image = _this.color + "Enemy" + _this.number;

        _this.number == 3 ? direction = "down" : null;
        _this.number == 0 ? direction = "up" : null;
    }, 1000 / 5);
}

/***/ }),

/***/ "./src/js/Enemies/Rock.js":
/*!********************************!*\
  !*** ./src/js/Enemies/Rock.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Rock;
function Rock(images, delay) {
    var _this = this;

    var canvas = document.getElementById("canvas");
    var context = canvas.getContext("2d");

    this.name = "rock";
    this.lives = 1000;
    this.x = 800;
    this.y = Math.random() * 300 + 400;
    this.number = 0;
    this.image = "rock" + this.number;
    this.width = 66;
    this.height = 66;
    this.start = false;

    this.update = function () {
        if (_this.start) {
            context.save();
            context.drawImage(images[_this.image], _this.x, _this.y);
            context.restore();
            _this.x -= 8;
            _this.y -= stepY;
        }
    };

    this.delete = function () {
        _this.x == -1000;
    };

    var stepY = Math.round(Math.random()) * 5 + 2;
    var direction = "up";

    //animation interval
    var interval = setInterval(function () {
        if (_this.start) {
            if (direction == "up") {
                _this.number++;
                _this.x -= 10;
            } else {
                _this.number--;
                _this.x += 10;
            }

            _this.image = "rock" + _this.number;

            _this.number == 5 ? direction = "down" : null;
            _this.number == 0 ? direction = "up" : null;
        }
    }, 1000 / 5);

    setTimeout(function () {
        _this.start = true;
    }, delay);
}

/***/ }),

/***/ "./src/js/Enemies/SnakeEnemy.js":
/*!**************************************!*\
  !*** ./src/js/Enemies/SnakeEnemy.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = SnakeEnemy;
function SnakeEnemy(x, y, images, delay) {
    var _this = this;

    var canvas = document.getElementById("canvas");
    var context = canvas.getContext("2d");

    this.name = "snake";
    this.lives = 2;
    this.invisible = false;
    this.x = x;
    this.y = y;
    this.width = 88;
    this.height = 56;
    this.pictureNumber = 0;
    this.image = "snake" + this.pictureNumber;

    this.update = function () {
        context.save();
        context.drawImage(images[_this.image], _this.x, _this.y);
        context.restore();
        direction == null ? _this.x -= 1 : null;
    };

    this.delete = function () {
        _this.invisible = true;
        clearInterval(imageInterval);
        setTimeout(function () {
            _this.image = 'dead0';
        }, 200);
        setTimeout(function () {
            _this.image = 'dead1';
        }, 400);
        setTimeout(function () {
            _this.image = 'dead2';
        }, 600);
        setTimeout(function () {
            _this.image = 'dead3';
        }, 800);
        setTimeout(function () {
            _this.image = 'dead4';
        }, 1000);
        setTimeout(function () {
            _this.x = -1000;
        }, 1200);
    };

    var picture = "up";
    var direction = "down";
    var imageInterval = null;
    //picture animation
    setTimeout(function () {
        imageInterval = setInterval(function () {
            if (picture == "up") {
                _this.pictureNumber++;
                _this.y -= 5;
            } else {
                _this.pictureNumber--;
                _this.y += 5;
            }

            _this.image = "snake" + _this.pictureNumber;

            _this.pictureNumber == 3 ? picture = "down" : null;
            _this.pictureNumber == 0 ? picture = "up" : null;
        }, 1000 / 5);
    }, delay);

    //move interval
    var interval = setInterval(function () {
        if (direction == "down") {
            _this.y += 2;
        } else if (direction == "left") {
            _this.x -= 2;
        } else if (direction == "up") {
            _this.y -= 2;
        }

        _this.y >= 600 ? direction = "left" : null;
        _this.x <= 600 ? direction = "up" : null;
        _this.y <= 100 ? direction = "left" : null;
    }, 1000 / 180);
}

/***/ }),

/***/ "./src/js/Enemies/WormEnemy.js":
/*!*************************************!*\
  !*** ./src/js/Enemies/WormEnemy.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = WormEnemy;
function WormEnemy(x, y, images, image, delay) {
    var _this = this;

    var canvas = document.getElementById("canvas");
    var context = canvas.getContext("2d");

    this.name = "worm";
    this.lives = 3;
    this.invisible = false;
    this.x = x;
    this.y = y;
    this.width = 68;
    this.height = 71;
    this.number = 0;
    this.image = image;

    this.update = function () {
        context.save();
        context.drawImage(images[_this.image], _this.x, _this.y);
        context.restore();
        _this.x -= 1.5;
    };

    this.delete = function () {
        _this.invisible = true;
        clearInterval(interval);
        setTimeout(function () {
            _this.image = 'dead0';
        }, 200);
        setTimeout(function () {
            _this.image = 'dead1';
        }, 400);
        setTimeout(function () {
            _this.image = 'dead2';
        }, 600);
        setTimeout(function () {
            _this.image = 'dead3';
        }, 800);
        setTimeout(function () {
            _this.image = 'dead4';
        }, 1000);
        setTimeout(function () {
            _this.x = -1000;
        }, 1200);
    };

    var direction = "up";
    var interval = null;

    setTimeout(function () {
        interval = setInterval(function () {
            if (direction == "up") {
                _this.number++;
                _this.x -= 4;
            } else {
                _this.number--;
                _this.x += 4;
            }

            _this.number == 30 ? direction = "down" : null;
            _this.number == 0 ? direction = "up" : null;
        }, 1000 / 60);
    }, delay);
}

/***/ }),

/***/ "./src/js/EnemyBullet.js":
/*!*******************************!*\
  !*** ./src/js/EnemyBullet.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = EnemyBullet;
function EnemyBullet(x, y, settings) {
    var _this = this;

    this.x = x;
    this.y = y;
    this.width = 26;
    this.height = 20;

    this.updatePosition = function () {
        _this.x -= settings.bulletSpeed;
    };

    this.deleteBullet = function () {
        _this.x = -1000;
    };
}

/***/ }),

/***/ "./src/js/Generator.js":
/*!*****************************!*\
  !*** ./src/js/Generator.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.default = Generator;

var _PlayerBullet = __webpack_require__(/*! ./PlayerBullet */ "./src/js/PlayerBullet.js");

var _PlayerBullet2 = _interopRequireDefault(_PlayerBullet);

var _EnemyBullet = __webpack_require__(/*! ./EnemyBullet */ "./src/js/EnemyBullet.js");

var _EnemyBullet2 = _interopRequireDefault(_EnemyBullet);

var _Collision = __webpack_require__(/*! ./Collision */ "./src/js/Collision.js");

var _Collision2 = _interopRequireDefault(_Collision);

var _Enemies = __webpack_require__(/*! ./Enemies.js */ "./src/js/Enemies.js");

var _Enemies2 = _interopRequireDefault(_Enemies);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Generator(canvas, player, settings, images) {
    var _this = this;

    var collision = new _Collision2.default(canvas, settings, player);
    var enemies = new _Enemies2.default(images, canvas);

    var enemiesCount = 0;
    var playerBulletsArray = [];
    var enemiesArray = [];
    var enemiesBulletsArray = [];

    this.start = function () {
        draw();
        addPlayerBullet();
    };

    var draw = function draw() {
        if (settings.start === true) {
            canvas.clearCanvas();
            canvas.drawPlayfield();
            player.updatePlayerPosition();
            canvas.drawPlayer();

            playerBulletsArray ? updatePlayerBullets() : null;
            canvas.drawPlayerBullets(playerBulletsArray);
            enemiesBulletsArray ? updateEnemiesBullets() : null;
            canvas.drawEnemiesBullets(enemiesBulletsArray);

            collision.checkCollision(player);
            collision.checkPlayerBulletsCollision(enemiesArray, playerBulletsArray);
            collision.checkEnemiesBulletsCollision(player, enemiesBulletsArray);
            collision.checkPlayerCollisionWithEnemies(player, enemiesArray);

            addEnemies();
            updateEnemies();

            canvas.drawScore();
        } else if (settings.start == "END") {
            canvas.drawScreen("END");
        } else if (settings.start == "OVER") {
            canvas.drawScreen("OVER");
        }

        requestAnimationFrame(draw);
    };

    var addEnemies = function addEnemies() {
        if (enemiesCount == 0 && canvas.playfieldX <= -500) {
            _this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 100);
            enemiesCount++;
            addEnemiesBullet(_this.enemiesArray);
        } else if (enemiesCount == 1 && canvas.playfieldX <= -4000 && canvas.run == 1) {
            _this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 300);
            enemiesCount++;
        } else if (enemiesCount == 2 && canvas.playfieldX <= -8500 && canvas.run == 1) {
            _this.enemiesArray = enemies.addFlyEnemies(enemiesArray, 20);
            enemiesCount++;
        } else if (enemiesCount == 3 && canvas.playfieldX <= -11500 && canvas.run == 1) {
            _this.enemiesArray = enemies.addRocks(enemiesArray);
            enemiesCount++;
        } else if (enemiesCount == 4 && canvas.playfieldX <= -14500 && canvas.run == 1) {
            _this.enemiesArray = enemies.addWormEnemies(enemiesArray, 200);
            enemiesCount++;
        } else if (enemiesCount == 5 && canvas.playfieldX <= -18500 && canvas.run == 1) {
            _this.enemiesArray = enemies.addSnakeEnemies(enemiesArray, 100);
            enemiesCount++;
        } else if (enemiesCount == 6 && canvas.playfieldX <= -20500 && canvas.run == 1) {
            _this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 100);
            enemiesCount++;
        } else if (enemiesCount == 7 && canvas.playfieldX <= -21000 && canvas.run == 1) {
            _this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 400);
            enemiesCount++;
        } else if (enemiesCount == 8 && canvas.playfieldX <= -23000 && canvas.run == 1) {
            _this.enemiesArray = enemies.addFlyEnemies(enemiesArray, 20);
            enemiesCount++;
        } else if (enemiesCount == 9 && canvas.playfieldX <= -1000 && canvas.run == 2) {
            _this.enemiesArray = enemies.addFlyEnemies(enemiesArray, 20);
            enemiesCount++;
        } else if (enemiesCount == 10 && canvas.playfieldX <= -3000 && canvas.run == 2) {
            _this.enemiesArray = enemies.addWormEnemies(enemiesArray, 200);
            enemiesCount++;
        } else if (enemiesCount == 11 && canvas.playfieldX <= -4000 && canvas.run == 2) {
            _this.enemiesArray = enemies.addRocks(enemiesArray);
            enemiesCount++;
        } else if (enemiesCount == 12 && canvas.playfieldX <= -7500 && canvas.run == 2) {
            _this.enemiesArray = enemies.addSnakeEnemies(enemiesArray, 100);
            enemiesCount++;
        } else if (enemiesCount == 13 && canvas.playfieldX <= -9000 && canvas.run == 2) {
            _this.enemiesArray = enemies.addSnakeEnemies(enemiesArray, 100);
            enemiesCount++;
        } else if (enemiesCount == 14 && canvas.playfieldX <= -11000 && canvas.run == 2) {
            _this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 100);
            _this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 300);
            _this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 500);
            enemiesCount++;
        } else if (enemiesCount == 15 && canvas.playfieldX <= -14000 && canvas.run == 2) {
            _this.enemiesArray = enemies.addWormEnemies(enemiesArray, 0);
            setTimeout(function () {
                _this.enemiesArray = enemies.addWormEnemies(enemiesArray, 300);
            }, 1000);
            enemiesCount++;
        } else if (enemiesCount == 16 && canvas.playfieldX <= -17000 && canvas.run == 2) {
            _this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 0);
            _this.enemiesArray = enemies.addSnakeEnemies(enemiesArray, 100);
            _this.enemiesArray = enemies.addWormEnemies(enemiesArray, 300);
            _this.enemiesArray = enemies.addRocks(enemiesArray);
            enemiesCount++;
        } else if (enemiesCount == 17 && canvas.playfieldX <= -21000 && canvas.run == 2) {
            settings.start = "END";
            enemiesCount++;
        }
    };

    var updateEnemies = function updateEnemies() {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = enemiesArray.entries()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var _step$value = _slicedToArray(_step.value, 2),
                    index = _step$value[0],
                    enemy = _step$value[1];

                enemy.update();
                enemy.x <= 0 - enemy.width ? enemiesArray.splice(index, 1) : null;
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }
    };

    var addPlayerBullet = function addPlayerBullet() {
        player.invisible == false ? playerBulletsArray.push(new _PlayerBullet2.default(player.x + player.width, player.y + player.height / 2, settings)) : null;
        setTimeout(function () {
            addPlayerBullet();
        }, settings.bulletsTime);
    };

    var updatePlayerBullets = function updatePlayerBullets() {
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
            for (var _iterator2 = playerBulletsArray.entries()[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var _step2$value = _slicedToArray(_step2.value, 2),
                    index = _step2$value[0],
                    bullet = _step2$value[1];

                bullet.updatePosition();
                bullet.x >= 800 ? playerBulletsArray.splice(index, 1) : null;
            }
        } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                    _iterator2.return();
                }
            } finally {
                if (_didIteratorError2) {
                    throw _iteratorError2;
                }
            }
        }
    };

    var addEnemiesBullet = function addEnemiesBullet(enemiesArray) {
        var _iteratorNormalCompletion3 = true;
        var _didIteratorError3 = false;
        var _iteratorError3 = undefined;

        try {
            var _loop = function _loop() {
                var enemy = _step3.value;

                if (enemy.name == "normal" || enemy.name == "worm") {
                    setTimeout(function () {
                        !enemy.invisible ? enemiesBulletsArray.push(new _EnemyBullet2.default(enemy.x, enemy.y + enemy.height / 2, settings)) : null;
                    }, Math.random() * 1000);
                }
            };

            for (var _iterator3 = enemiesArray[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                _loop();
            }
        } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion3 && _iterator3.return) {
                    _iterator3.return();
                }
            } finally {
                if (_didIteratorError3) {
                    throw _iteratorError3;
                }
            }
        }

        setTimeout(function () {
            addEnemiesBullet(enemiesArray);
        }, 4000);
    };

    var updateEnemiesBullets = function updateEnemiesBullets() {
        var _iteratorNormalCompletion4 = true;
        var _didIteratorError4 = false;
        var _iteratorError4 = undefined;

        try {
            for (var _iterator4 = enemiesBulletsArray.entries()[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                var _step4$value = _slicedToArray(_step4.value, 2),
                    index = _step4$value[0],
                    bullet = _step4$value[1];

                bullet.updatePosition();
                bullet.x <= 0 ? enemiesBulletsArray.splice(index, 1) : null;
            }
        } catch (err) {
            _didIteratorError4 = true;
            _iteratorError4 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion4 && _iterator4.return) {
                    _iterator4.return();
                }
            } finally {
                if (_didIteratorError4) {
                    throw _iteratorError4;
                }
            }
        }
    };
}

/***/ }),

/***/ "./src/js/Images.js":
/*!**************************!*\
  !*** ./src/js/Images.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Images;
function Images() {
    this.blueEnemy0 = new Image();
    this.blueEnemy1 = new Image();
    this.blueEnemy2 = new Image();
    this.blueEnemy3 = new Image();
    this.bluePoint = new Image();
    this.bullet = new Image();
    this.dead0 = new Image();
    this.dead1 = new Image();
    this.dead2 = new Image();
    this.dead3 = new Image();
    this.dead4 = new Image();
    this.enemyBullet = new Image();
    this.grayEnemy0 = new Image();
    this.grayEnemy1 = new Image();
    this.grayEnemy2 = new Image();
    this.grayEnemy3 = new Image();
    this.grayFly0 = new Image();
    this.grayFly1 = new Image();
    this.grayFly2 = new Image();
    this.grayFly3 = new Image();
    this.player = new Image();
    this.playerT = new Image();
    this.playfield = new Image();
    this.redEnemy0 = new Image();
    this.redEnemy1 = new Image();
    this.redEnemy2 = new Image();
    this.redEnemy3 = new Image();
    this.redPoint = new Image();
    this.rock0 = new Image();
    this.rock1 = new Image();
    this.rock2 = new Image();
    this.rock3 = new Image();
    this.rock4 = new Image();
    this.rock5 = new Image();
    this.snake0 = new Image();
    this.snake1 = new Image();
    this.snake2 = new Image();
    this.snake3 = new Image();
    this.wormBody = new Image();
    this.wormHead = new Image();
    this.gameOver = new Image();
    this.theEnd = new Image();
}

/***/ }),

/***/ "./src/js/Loading.js":
/*!***************************!*\
  !*** ./src/js/Loading.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Loading;
function Loading(images) {
    var _this = this;

    this.fileCounter = 0;
    this.endOfLoading = false;

    var displayAllFiles = function displayAllFiles() {
        for (var img in images) {
            console.log(images[img]);
        }
    };

    //count all files to load
    (function () {
        for (var img in images) {
            _this.fileCounter++;
        }
        console.log("Amount of files: " + _this.fileCounter);
    })();

    //add src to image (load files)
    (function () {
        for (var img in images) {
            images[img].src = "gfx/" + img + ".png";
            images[img].onload = function () {
                _this.fileCounter--;
                _this.fileCounter == 0 ? _this.endOfLoading = true : null;
                _this.endOfLoading ? console.log("Loaded all files") : null;
                // this.endOfLoading ? displayAllFiles() : null;
            };
        }
    })();
}

/***/ }),

/***/ "./src/js/Main.js":
/*!************************!*\
  !*** ./src/js/Main.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Images = __webpack_require__(/*! ./Images.js */ "./src/js/Images.js");

var _Images2 = _interopRequireDefault(_Images);

var _Loading = __webpack_require__(/*! ./Loading.js */ "./src/js/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _Canvas = __webpack_require__(/*! ./Canvas.js */ "./src/js/Canvas.js");

var _Canvas2 = _interopRequireDefault(_Canvas);

var _Generator = __webpack_require__(/*! ./Generator.js */ "./src/js/Generator.js");

var _Generator2 = _interopRequireDefault(_Generator);

var _Settings = __webpack_require__(/*! ./Settings.js */ "./src/js/Settings.js");

var _Settings2 = _interopRequireDefault(_Settings);

var _Player = __webpack_require__(/*! ./Player */ "./src/js/Player.js");

var _Player2 = _interopRequireDefault(_Player);

var _Ui = __webpack_require__(/*! ./Ui.js */ "./src/js/Ui.js");

var _Ui2 = _interopRequireDefault(_Ui);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

__webpack_require__(/*! ./../scss/style.scss */ "./src/scss/style.scss");

var settings = new _Settings2.default();
var images = new _Images2.default();
var loading = new _Loading2.default(images);
var player = new _Player2.default(settings);
var canvas = new _Canvas2.default(images, settings, player);
var generator = new _Generator2.default(canvas, player, settings, images);
var ui = new _Ui2.default(player);

var interval = setInterval(function () {
    if (loading.endOfLoading) {
        clearInterval(interval);
        generator.start();
    }
}, 100);

/***/ }),

/***/ "./src/js/Player.js":
/*!**************************!*\
  !*** ./src/js/Player.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Player;
function Player(settings) {
    var _this = this;

    this.lives = 3;
    this.invisible = false;
    this.x = 50;
    this.y = 350;
    this.width = 80;
    this.height = 66;
    this.direction = null;
    this.image = 'player';
    this.score = 0;

    this.updatePlayerPosition = function () {
        if (_this.invisible == false) {
            switch (_this.direction) {
                case "left":
                    moveLeft();
                    break;
                case "right":
                    moveRight();
                    break;
                case "up":
                    moveUp();
                    break;
                case "down":
                    moveDown();
                    break;
            }
        }
    };

    this.dead = function () {
        if (_this.lives >= 0) {
            _this.lives--;
            settings.bulletsTime = 500;
            _this.invisible = true;
            setTimeout(function () {
                _this.image = 'dead0';
            }, 200);
            setTimeout(function () {
                _this.image = 'dead1';
            }, 400);
            setTimeout(function () {
                _this.image = 'dead2';
            }, 600);
            setTimeout(function () {
                _this.image = 'dead3';
            }, 800);
            setTimeout(function () {
                _this.image = 'dead4';
            }, 1000);
            setTimeout(function () {
                _this.x = 50;
                _this.y = 350;
                _this.image = 'playerT';
                setTimeout(function () {
                    _this.invisible = false;
                    _this.image = 'player';
                }, 1000);
            }, 1200);
        }
        if (_this.lives == 0) {
            setTimeout(function () {
                settings.start = "OVER";
            }, 1200);
        }
    };

    var moveUp = function moveUp() {
        _this.y >= settings.playerSpeed ? _this.y -= settings.playerSpeed : null;
    };

    var moveDown = function moveDown() {
        _this.y <= 700 - _this.height ? _this.y += settings.playerSpeed : null;
    };

    var moveRight = function moveRight() {
        _this.x <= 800 - _this.width - settings.playerSpeed ? _this.x += settings.playerSpeed : null;
    };

    var moveLeft = function moveLeft() {
        _this.x >= settings.playerSpeed ? _this.x -= settings.playerSpeed : null;
    };
}

/***/ }),

/***/ "./src/js/PlayerBullet.js":
/*!********************************!*\
  !*** ./src/js/PlayerBullet.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = PlayerBullet;
function PlayerBullet(x, y, settings) {
    var _this = this;

    this.x = x;
    this.y = y;
    this.width = 27;
    this.height = 27;

    this.updatePosition = function () {
        _this.x += settings.bulletSpeed;
    };

    this.deleteBullet = function () {
        _this.x = 1000;
    };
}

/***/ }),

/***/ "./src/js/Settings.js":
/*!****************************!*\
  !*** ./src/js/Settings.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Settings;
function Settings() {
    this.playfieldWidth = 24500;
    this.playerSpeed = 7;
    this.bulletsTime = 500;
    this.bulletSpeed = 14;
    this.start = true;
}

/***/ }),

/***/ "./src/js/Ui.js":
/*!**********************!*\
  !*** ./src/js/Ui.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Ui;
function Ui(player) {
    document.addEventListener("keydown", function (e) {
        switch (e.keyCode) {
            case 87:
                player.direction = "up";
                break;
            case 65:
                player.direction = "left";
                break;
            case 83:
                player.direction = "down";
                break;
            case 68:
                player.direction = "right";
                break;
        }
    });

    document.addEventListener("keyup", function (e) {
        e.keyCode === 87 && player.direction == "up" ? player.direction = null : null;
        e.keyCode === 65 && player.direction == "left" ? player.direction = null : null;
        e.keyCode === 83 && player.direction == "down" ? player.direction = null : null;
        e.keyCode === 68 && player.direction == "right" ? player.direction = null : null;
    });
}

/***/ }),

/***/ "./src/scss/style.scss":
/*!*****************************!*\
  !*** ./src/scss/style.scss ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader!../../node_modules/sass-loader/lib/loader.js!./style.scss */ "./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./src/scss/style.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map