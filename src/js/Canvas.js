export default function Canvas(image, settings, player) {
    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");

    this.playfieldX = 0;
    this.run = 1;

    this.clearCanvas = () => {
        context.clearRect(0, 0, 800, 700);
        context.fillStyle = 'black';
        context.fillRect(0, 0, 800, 700);
    }

    this.drawPlayfield = () => {
        context.save();
        context.drawImage(image.playfield, this.playfieldX, 0, settings.playfieldWidth, 700);
        context.restore();

        this.playfieldX -= settings.playerSpeed;

        this.playfieldX <= -(24500 - 800) ? this.run++ : null;
        this.playfieldX <= -(24500 - 800) ? player.score += 1000 : null;
        this.playfieldX <= -(24500 - 800) ? this.playfieldX = 0 : null;
    }

    this.drawPlayer = () => {
        context.save();
        context.drawImage(image[player.image], player.x, player.y);
        context.restore();
    }

    this.drawPlayerBullets = (bulletsArray) => {
        for (const bullet of bulletsArray) {
            context.save();
            context.drawImage(image.bullet, bullet.x, bullet.y);
            context.restore();
        }
    }

    this.drawEnemiesBullets = (bulletsArray) => {
        for (const bullet of bulletsArray) {
            context.save();
            context.drawImage(image.enemyBullet, bullet.x, bullet.y);
            context.restore();
        }
    }

    this.drawScreen = (screen) => {
        if (screen == "END") {
            context.save();
            context.drawImage(image.theEnd, 0, 0);
            context.restore();
        } else if (screen == "OVER") {
            context.drawImage(image.gameOver, 0, 0);
        }
    }

    this.drawScore = () => {
        context.save();
        context.font = "15px Lucida Console";
        context.fillStyle = "white";
        context.fillText(`Score: ${player.score} ❤: ${player.lives}`, 10, 20);
        context.restore();
    }
}