export default function Collision(canvas, settings, player) {
    this.playfieldArray = [
        { name: "ground", from: { x: 0, y: 685 }, to: { x: 24500, y: 700 } },
        { name: "flower", from: { x: 1073, y: 607 }, to: { x: 1165, y: 700 } },
        { name: "flower", from: { x: 1285, y: 607 }, to: { x: 1377, y: 700 } },
        { name: "flower", from: { x: 1607, y: 607 }, to: { x: 1700, y: 700 } },
        { name: "flower", from: { x: 1707, y: 413 }, to: { x: 1812, y: 700 } },
        { name: "flower", from: { x: 1812, y: 507 }, to: { x: 1916, y: 700 } },
        { name: "mushroom", from: { x: 2120, y: 600 }, to: { x: 2265, y: 700 } },
        { name: "mushroom", from: { x: 2870, y: 600 }, to: { x: 3000, y: 700 } },
        { name: "flower", from: { x: 4706, y: 607 }, to: { x: 4800, y: 700 } },
        { name: "pyramid", from: { x: 4808, y: 670 }, to: { x: 5080, y: 700 } },
        { name: "pyramid", from: { x: 4841, y: 644 }, to: { x: 5050, y: 670 } },
        { name: "pyramid", from: { x: 4870, y: 624 }, to: { x: 5026, y: 644 } },
        { name: "flower", from: { x: 4895, y: 511 }, to: { x: 5000, y: 624 } },
        { name: "highground", from: { x: 5155, y: 660 }, to: { x: 5235, y: 700 } },
        { name: "highground", from: { x: 5176, y: 636 }, to: { x: 5235, y: 700 } },
        { name: "highground", from: { x: 5208, y: 607 }, to: { x: 5235, y: 700 } },
        { name: "highground", from: { x: 5235, y: 581 }, to: { x: 7878, y: 700 } },
        { name: "bar", from: { x: 5641, y: 525 }, to: { x: 5679, y: 700 } },
        { name: "bar", from: { x: 5850, y: 525 }, to: { x: 5891, y: 700 } },
        { name: "bar", from: { x: 5953, y: 525 }, to: { x: 5993, y: 700 } },
        { name: "mushroom", from: { x: 6005, y: 500 }, to: { x: 6177, y: 700 } },
        { name: "bar", from: { x: 6874, y: 525 }, to: { x: 6911, y: 700 } },
        { name: "bar", from: { x: 7074, y: 525 }, to: { x: 7118, y: 700 } },
        { name: "bar", from: { x: 7181, y: 525 }, to: { x: 7220, y: 700 } },
        { name: "mushroom", from: { x: 7231, y: 500 }, to: { x: 7402, y: 700 } },
        { name: "highground", from: { x: 7878, y: 597 }, to: { x: 7901, y: 700 } },
        { name: "highground", from: { x: 7901, y: 623 }, to: { x: 7928, y: 700 } },
        { name: "highground", from: { x: 7928, y: 6480 }, to: { x: 7954, y: 700 } },
        { name: "flower", from: { x: 9913, y: 502 }, to: { x: 10016, y: 624 } },
        { name: "mushroom", from: { x: 10059, y: 600 }, to: { x: 10208, y: 700 } },
        { name: "mushroom", from: { x: 10266, y: 600 }, to: { x: 10419, y: 700 } },
        { name: "mushroom", from: { x: 12250, y: 600 }, to: { x: 12404, y: 700 } },
        { name: "flower", from: { x: 12916, y: 600 }, to: { x: 13007, y: 700 } },
        { name: "flower", from: { x: 13126, y: 600 }, to: { x: 13216, y: 700 } },
        { name: "flower", from: { x: 13449, y: 600 }, to: { x: 13542, y: 700 } },
        { name: "flower", from: { x: 13449, y: 600 }, to: { x: 13653, y: 700 } },
        { name: "flower", from: { x: 13653, y: 509 }, to: { x: 13762, y: 700 } },
        { name: "flower", from: { x: 14304, y: 509 }, to: { x: 14403, y: 700 } },
        { name: "mushroom", from: { x: 14649, y: 600 }, to: { x: 14805, y: 700 } },
        { name: "mushroom", from: { x: 14922, y: 600 }, to: { x: 15076, y: 700 } },
        { name: "flower", from: { x: 15375, y: 607 }, to: { x: 15469, y: 700 } },
        { name: "flower", from: { x: 15588, y: 607 }, to: { x: 15680, y: 700 } },
        { name: "flower", from: { x: 15914, y: 607 }, to: { x: 16005, y: 700 } },
        { name: "flower", from: { x: 16012, y: 418 }, to: { x: 16115, y: 700 } },
        { name: "flower", from: { x: 16115, y: 511 }, to: { x: 16227, y: 700 } },
        { name: "pyramid", from: { x: 18942, y: 646 }, to: { x: 19260, y: 700 } },
        { name: "pyramid", from: { x: 18976, y: 594 }, to: { x: 19225, y: 700 } },
        { name: "pyramid", from: { x: 19030, y: 540 }, to: { x: 19172, y: 700 } },
        { name: "pyramid", from: { x: 19073, y: 512 }, to: { x: 19129, y: 700 } },
        { name: "pyramid", from: { x: 18942 + 960, y: 646 }, to: { x: 19260 + 960, y: 700 } },
        { name: "pyramid", from: { x: 18976 + 960, y: 594 }, to: { x: 19225 + 960, y: 700 } },
        { name: "pyramid", from: { x: 19030 + 960, y: 540 }, to: { x: 19172 + 960, y: 700 } },
        { name: "pyramid", from: { x: 19073 + 960, y: 512 }, to: { x: 19129 + 960, y: 700 } },
        { name: "pyramid", from: { x: 18942 + 2139, y: 646 }, to: { x: 19260 + 2139, y: 700 } },
        { name: "pyramid", from: { x: 18976 + 2139, y: 594 }, to: { x: 19225 + 2139, y: 700 } },
        { name: "pyramid", from: { x: 19030 + 2139, y: 540 }, to: { x: 19172 + 2139, y: 700 } },
        { name: "pyramid", from: { x: 19073 + 2139, y: 512 }, to: { x: 19129 + 2139, y: 700 } },
        { name: "pyramid", from: { x: 18942 + 2676, y: 646 }, to: { x: 19260 + 2676, y: 700 } },
        { name: "pyramid", from: { x: 18976 + 2676, y: 594 }, to: { x: 19225 + 2676, y: 700 } },
        { name: "pyramid", from: { x: 19030 + 2676, y: 540 }, to: { x: 19172 + 2676, y: 700 } },
        { name: "pyramid", from: { x: 19073 + 2676, y: 512 }, to: { x: 19129 + 2676, y: 700 } },
        { name: "pyramid", from: { x: 18942 + 3209, y: 646 }, to: { x: 19260 + 3209, y: 700 } },
        { name: "pyramid", from: { x: 18976 + 3209, y: 594 }, to: { x: 19225 + 3209, y: 700 } },
        { name: "pyramid", from: { x: 19030 + 3209, y: 540 }, to: { x: 19172 + 3209, y: 700 } },
        { name: "pyramid", from: { x: 19073 + 3209, y: 512 }, to: { x: 19129 + 3209, y: 700 } },
        { name: "pyramid", from: { x: 18942 + 3742, y: 646 }, to: { x: 19260 + 3742, y: 700 } },
        { name: "pyramid", from: { x: 18976 + 3742, y: 594 }, to: { x: 19225 + 3742, y: 700 } },
        { name: "pyramid", from: { x: 19030 + 3742, y: 540 }, to: { x: 19172 + 3742, y: 700 } },
        { name: "pyramid", from: { x: 19073 + 3742, y: 512 }, to: { x: 19129 + 3742, y: 700 } },
    ]

    this.checkCollision = (player) => {
        if (!player.invisible) {
            const playerX = player.x + player.width - canvas.playfieldX;
            const playerY = player.y + player.height;
            for (const collision of this.playfieldArray) {
                if (playerX >= collision.from.x && playerX <= collision.to.x &&
                    playerY >= collision.from.y && playerY <= collision.to.y) {
                    player.dead();
                }
            }
        }
    }

    this.checkPlayerBulletsCollision = (enemiesArray, playerBullets) => {
        for (let bullet of playerBullets) {
            const bulletX = bullet.x + bullet.width;
            const bulletY = bullet.y + (bullet.height / 2);
            for (let enemy of enemiesArray) {
                if (enemy.invisible == false) {
                    if (bulletX >= enemy.x && bulletX <= (enemy.x + enemy.width) &&
                        bulletY >= enemy.y && bulletY <= (enemy.y + enemy.height)) {
                        bullet.deleteBullet();
                        enemy.lives--;
                        if (enemy.lives <= 0) {
                            enemy.delete();
                            player.score += 100;
                            settings.bulletsTime >= 150 ? settings.bulletsTime -= 10 : null;
                        }
                    }
                }
            }
        }
    }

    this.checkEnemiesBulletsCollision = (player, enemiesBullets) => {
        for (let bullet of enemiesBullets) {
            const bulletX = bullet.x;
            const bulletY = bullet.y + (bullet.height / 2);
            if (!player.invisible) {
                if (bulletX >= player.x && bulletX <= (player.x + player.width) &&
                    bulletY >= player.y && bulletY <= (player.y + player.height)) {
                    bullet.deleteBullet();
                    player.dead();
                }
            }
        }
    }

    this.checkPlayerCollisionWithEnemies = (player, enemiesArray) => {
        const playerX = player.x + player.width;
        const playerTopY = player.y;
        const playerBottomY = player.y + player.height;
        for (let enemy of enemiesArray) {
            if (!player.invisible && !enemy.invisible) {
                if (playerX >= enemy.x && playerX <= enemy.x + enemy.width &&
                    playerTopY >= enemy.y && playerTopY <= enemy.y + enemy.height ||
                    playerX >= enemy.x && playerX <= enemy.x + enemy.width &&
                    playerBottomY >= enemy.y && playerBottomY <= enemy.y + enemy.height) {
                    player.dead();
                }
            }
        }
    }
}