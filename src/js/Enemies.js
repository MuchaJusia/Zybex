import NormalEnemy from './Enemies/NormalEnemy.js';
import FlyEnemy from './Enemies/FlyEnemy.js';
import WormEnemy from './Enemies/WormEnemy.js';
import SnakeEnemy from './Enemies/SnakeEnemy.js';
import Rock from './Enemies/Rock.js';

export default function Enemies(images, canvas) {
    this.addNormalEnemies = (enemiesArray, height) => {
        for (let i = 0; i < 5; i++) {
            let color = null;
            if (Math.round(Math.random()))
                color = "red";
            else
                color = "blue";

            enemiesArray.push(new NormalEnemy((Math.random() * 200) + 800, height + (40 * i), color, images));
        }
        return enemiesArray;
    }

    this.addFlyEnemies = (enemiesArray, height) => {
        for (let i = 0; i < 8; i++) {
            enemiesArray.push(new FlyEnemy(800, height + (80 * i), images));
        }

        return enemiesArray;
    }

    this.addWormEnemies = (enemiesArray, height) => {
        for (let i = 0; i < 5; i++) {
            i == 0 ? enemiesArray.push(new WormEnemy(800, height + (60 * i), images, "wormHead", i * 100)) : enemiesArray.push(new WormEnemy(800, height + (60 * i), images, "wormBody", i * 100));
        }

        return enemiesArray;
    }

    this.addSnakeEnemies = (enemiesArray, height) => {
        for (let i = 0; i < 8; i++) {
            enemiesArray.push(new SnakeEnemy(720, height + (50 * i), images, i * 150));
        }

        return enemiesArray;
    }

    this.addRocks = (enemiesArray) => {
        for (let i = 0; i < 25; i++) {
            enemiesArray.push(new Rock(images, i * 200));
        }

        return enemiesArray;
    }
}