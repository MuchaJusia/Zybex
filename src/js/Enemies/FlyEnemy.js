export default function FlyEnemy(x, y, images) {
    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");

    this.name = "fly";
    this.lives = 2;
    this.invisible = false;
    this.x = x;
    this.y = y;
    this.width = 70;
    this.height = 70;
    this.number = 0;
    this.image = `grayFly${this.number}`;
    this.speed = 1.5;

    this.update = () => {
        context.save();
        context.drawImage(images[this.image], this.x, this.y);
        context.restore();
        this.x -= this.speed;
    }

    this.delete = () => {
        this.invisible = true;
        clearInterval(interval);
        setTimeout(() => {
            this.image = 'dead0';
        }, 200);
        setTimeout(() => {
            this.image = 'dead1';
        }, 400);
        setTimeout(() => {
            this.image = 'dead2';
        }, 600);
        setTimeout(() => {
            this.image = 'dead3';
        }, 800);
        setTimeout(() => {
            this.image = 'dead4';
        }, 1000);
        setTimeout(() => {
            this.x = -1000;
        }, 1200);
    }

    setTimeout(() => {
        this.speed = 0;
    }, 1000);

    setTimeout(() => {
        this.speed = 14;
    }, (Math.random() * 1000) + 3000);

    let direction = "up";

    const interval = setInterval(() => {
        if (direction == "up") {
            this.number++;
        } else {
            this.number--;
        }

        this.image = `grayFly${this.number}`

        this.number == 3 ? direction = "down" : null;
        this.number == 0 ? direction = "up" : null;
    }, 1000 / 10);
}