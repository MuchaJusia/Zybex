export default function NormalEnemy(x, y, color, images) {
    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");

    this.name = "normal";
    this.lives = 2;
    this.invisible = false;
    this.x = x;
    this.y = y;
    this.width = 80;
    this.height = 69;
    this.color = color;
    this.number = 0;
    this.image = `${this.color}Enemy${this.number}`;

    this.update = () => {
        context.save();
        context.drawImage(images[this.image], this.x, this.y);
        context.restore();
        this.x -= 1.5;
    }

    this.delete = () => {
        this.invisible = true;
        clearInterval(interval);
        setTimeout(() => {
            this.image = 'dead0';
        }, 200);
        setTimeout(() => {
            this.image = 'dead1';
        }, 400);
        setTimeout(() => {
            this.image = 'dead2';
        }, 600);
        setTimeout(() => {
            this.image = 'dead3';
        }, 800);
        setTimeout(() => {
            this.image = 'dead4';
        }, 1000);
        setTimeout(() => {
            this.x = -1000;
        }, 1200);
    }

    let direction = "up";

    const interval = setInterval(() => {
        if (direction == "up") {
            this.number++;
            this.x -= 10;
        } else {
            this.number--;
            this.x += 10;
        }

        this.image = `${this.color}Enemy${this.number}`

        this.number == 3 ? direction = "down" : null;
        this.number == 0 ? direction = "up" : null;
    }, 1000 / 5);
}