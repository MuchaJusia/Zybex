export default function Rock(images, delay) {
    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");

    this.name = "rock";
    this.lives = 1000;
    this.x = 800;
    this.y = Math.random() * 300 + 400;
    this.number = 0;
    this.image = `rock${this.number}`
    this.width = 66;
    this.height = 66;
    this.start = false;
    
    this.update = () => {
        if (this.start) {
            context.save();
            context.drawImage(images[this.image], this.x, this.y);
            context.restore();
            this.x -= 8;
            this.y -= stepY;
        }
    }

    this.delete = () => {
        this.x == -1000;
    }

    let stepY = (Math.round(Math.random()) * 5) + 2;
    let direction = "up";

    //animation interval
    const interval = setInterval(() => {
        if (this.start) {
            if (direction == "up") {
                this.number++;
                this.x -= 10;
            } else {
                this.number--;
                this.x += 10;
            }

            this.image = `rock${this.number}`

            this.number == 5 ? direction = "down" : null;
            this.number == 0 ? direction = "up" : null;
        }
    }, 1000 / 5);

    setTimeout(() => {
        this.start = true;
    }, delay)
}