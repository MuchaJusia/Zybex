export default function SnakeEnemy(x, y, images, delay) {
    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");

    this.name = "snake";
    this.lives = 2;
    this.invisible = false;
    this.x = x;
    this.y = y;
    this.width = 88;
    this.height = 56;
    this.pictureNumber = 0;
    this.image = `snake${this.pictureNumber}`;

    this.update = () => {
        context.save();
        context.drawImage(images[this.image], this.x, this.y);
        context.restore();
        direction == null ? this.x -= 1 : null;
    }

    this.delete = () => {
        this.invisible = true;
        clearInterval(imageInterval);
        setTimeout(() => {
            this.image = 'dead0';
        }, 200);
        setTimeout(() => {
            this.image = 'dead1';
        }, 400);
        setTimeout(() => {
            this.image = 'dead2';
        }, 600);
        setTimeout(() => {
            this.image = 'dead3';
        }, 800);
        setTimeout(() => {
            this.image = 'dead4';
        }, 1000);
        setTimeout(() => {
            this.x = -1000;
        }, 1200);
    }

    let picture = "up";
    let direction = "down";
    let imageInterval = null;
    //picture animation
    setTimeout(() => {
        imageInterval = setInterval(() => {
            if (picture == "up") {
                this.pictureNumber++;
                this.y -= 5;
            } else {
                this.pictureNumber--;
                this.y += 5;
            }

            this.image = `snake${this.pictureNumber}`

            this.pictureNumber == 3 ? picture = "down" : null;
            this.pictureNumber == 0 ? picture = "up" : null;
        }, 1000 / 5);
    }, delay)

    //move interval
    const interval = setInterval(() => {
        if (direction == "down") {
            this.y += 2;
        } else if (direction == "left") {
            this.x -= 2;
        } else if (direction == "up") {
            this.y -= 2;
        }

        this.y >= 600 ? direction = "left" : null;
        this.x <= 600 ? direction = "up" : null;
        this.y <= 100 ? direction = "left" : null;

    }, 1000 / 180);
}