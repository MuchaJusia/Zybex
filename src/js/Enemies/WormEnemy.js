export default function WormEnemy(x, y, images, image, delay) {
    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");

    this.name = "worm";
    this.lives = 3;
    this.invisible = false;
    this.x = x;
    this.y = y;
    this.width = 68;
    this.height = 71;
    this.number = 0;
    this.image = image;

    this.update = () => {
        context.save();
        context.drawImage(images[this.image], this.x, this.y);
        context.restore();
        this.x -= 1.5;
    }

    this.delete = () => {
        this.invisible = true;
        clearInterval(interval);
        setTimeout(() => {
            this.image = 'dead0';
        }, 200);
        setTimeout(() => {
            this.image = 'dead1';
        }, 400);
        setTimeout(() => {
            this.image = 'dead2';
        }, 600);
        setTimeout(() => {
            this.image = 'dead3';
        }, 800);
        setTimeout(() => {
            this.image = 'dead4';
        }, 1000);
        setTimeout(() => {
            this.x = -1000;
        }, 1200);
    }

    let direction = "up";
    let interval = null;

    setTimeout(() => {
        interval = setInterval(() => {
            if (direction == "up") {
                this.number++;
                this.x -= 4;
            } else {
                this.number--;
                this.x += 4;
            }

            this.number == 30 ? direction = "down" : null;
            this.number == 0 ? direction = "up" : null;
        }, 1000 / 60);
    }, delay);
}