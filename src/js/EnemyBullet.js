export default function EnemyBullet(x, y, settings) {
    this.x = x;
    this.y = y;
    this.width = 26;
    this.height = 20;

    this.updatePosition = () => {
        this.x -= settings.bulletSpeed;
    }

    this.deleteBullet = () => {
        this.x = -1000;
    }
}