import PlayerBullet from "./PlayerBullet";
import EnemyBullet from "./EnemyBullet";
import Collision from "./Collision";
import Enemies from './Enemies.js';

export default function Generator(canvas, player, settings, images) {
    const collision = new Collision(canvas, settings, player);
    const enemies = new Enemies(images, canvas);

    let enemiesCount = 0;
    let playerBulletsArray = [];
    let enemiesArray = [];
    let enemiesBulletsArray = [];

    this.start = () => {
        draw();
        addPlayerBullet();
    }

    const draw = () => {
        if (settings.start === true) {
            canvas.clearCanvas();
            canvas.drawPlayfield();
            player.updatePlayerPosition();
            canvas.drawPlayer();

            playerBulletsArray ? updatePlayerBullets() : null;
            canvas.drawPlayerBullets(playerBulletsArray);
            enemiesBulletsArray ? updateEnemiesBullets() : null;
            canvas.drawEnemiesBullets(enemiesBulletsArray);

            collision.checkCollision(player);
            collision.checkPlayerBulletsCollision(enemiesArray, playerBulletsArray);
            collision.checkEnemiesBulletsCollision(player, enemiesBulletsArray);
            collision.checkPlayerCollisionWithEnemies(player, enemiesArray);

            addEnemies();
            updateEnemies();

            canvas.drawScore();
        } else if (settings.start == "END") {
            canvas.drawScreen("END");
        } else if (settings.start == "OVER") {
            canvas.drawScreen("OVER");
        }

        requestAnimationFrame(draw);
    }

    const addEnemies = () => {
        if (enemiesCount == 0 && canvas.playfieldX <= -500) {
            this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 100);
            enemiesCount++;
            addEnemiesBullet(this.enemiesArray);
        } else if (enemiesCount == 1 && canvas.playfieldX <= -4000 && canvas.run == 1) {
            this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 300);
            enemiesCount++;
        } else if (enemiesCount == 2 && canvas.playfieldX <= -8500 && canvas.run == 1) {
            this.enemiesArray = enemies.addFlyEnemies(enemiesArray, 20);
            enemiesCount++;
        } else if (enemiesCount == 3 && canvas.playfieldX <= -11500 && canvas.run == 1) {
            this.enemiesArray = enemies.addRocks(enemiesArray);
            enemiesCount++;
        } else if (enemiesCount == 4 && canvas.playfieldX <= -14500 && canvas.run == 1) {
            this.enemiesArray = enemies.addWormEnemies(enemiesArray, 200);
            enemiesCount++;
        } else if (enemiesCount == 5 && canvas.playfieldX <= -18500 && canvas.run == 1) {
            this.enemiesArray = enemies.addSnakeEnemies(enemiesArray, 100);
            enemiesCount++;
        } else if (enemiesCount == 6 && canvas.playfieldX <= -20500 && canvas.run == 1) {
            this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 100);
            enemiesCount++;
        } else if (enemiesCount == 7 && canvas.playfieldX <= -21000 && canvas.run == 1) {
            this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 400);
            enemiesCount++;
        } else if (enemiesCount == 8 && canvas.playfieldX <= -23000 && canvas.run == 1) {
            this.enemiesArray = enemies.addFlyEnemies(enemiesArray, 20);
            enemiesCount++;
        } else if (enemiesCount == 9 && canvas.playfieldX <= -1000 && canvas.run == 2) {
            this.enemiesArray = enemies.addFlyEnemies(enemiesArray, 20);
            enemiesCount++;
        } else if (enemiesCount == 10 && canvas.playfieldX <= -3000 && canvas.run == 2) {
            this.enemiesArray = enemies.addWormEnemies(enemiesArray, 200);
            enemiesCount++;
        } else if (enemiesCount == 11 && canvas.playfieldX <= -4000 && canvas.run == 2) {
            this.enemiesArray = enemies.addRocks(enemiesArray);
            enemiesCount++;
        } else if (enemiesCount == 12 && canvas.playfieldX <= -7500 && canvas.run == 2) {
            this.enemiesArray = enemies.addSnakeEnemies(enemiesArray, 100);
            enemiesCount++;
        } else if (enemiesCount == 13 && canvas.playfieldX <= -9000 && canvas.run == 2) {
            this.enemiesArray = enemies.addSnakeEnemies(enemiesArray, 100);
            enemiesCount++;
        } else if (enemiesCount == 14 && canvas.playfieldX <= -11000 && canvas.run == 2) {
            this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 100);
            this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 300);
            this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 500);
            enemiesCount++;
        } else if (enemiesCount == 15 && canvas.playfieldX <= -14000 && canvas.run == 2) {
            this.enemiesArray = enemies.addWormEnemies(enemiesArray, 0);
            setTimeout(() => {
                this.enemiesArray = enemies.addWormEnemies(enemiesArray, 300);
            }, 1000);
            enemiesCount++;
        } else if (enemiesCount == 16 && canvas.playfieldX <= -17000 && canvas.run == 2) {
            this.enemiesArray = enemies.addNormalEnemies(enemiesArray, 0);
            this.enemiesArray = enemies.addSnakeEnemies(enemiesArray, 100);
            this.enemiesArray = enemies.addWormEnemies(enemiesArray, 300);
            this.enemiesArray = enemies.addRocks(enemiesArray);
            enemiesCount++;
        } else if (enemiesCount == 17 && canvas.playfieldX <= -21000 && canvas.run == 2) {
            settings.start = "END";
            enemiesCount++;
        }
    }

    const updateEnemies = () => {
        for (const [index, enemy] of enemiesArray.entries()) {
            enemy.update();
            enemy.x <= 0 - enemy.width ? enemiesArray.splice(index, 1) : null;
        }
    }

    const addPlayerBullet = () => {
        player.invisible == false ? playerBulletsArray.push(new PlayerBullet(player.x + player.width, player.y + (player.height / 2), settings)) : null;
        setTimeout(function () {
            addPlayerBullet();
        }, settings.bulletsTime);
    }

    const updatePlayerBullets = () => {
        for (const [index, bullet] of playerBulletsArray.entries()) {
            bullet.updatePosition();
            bullet.x >= 800 ? playerBulletsArray.splice(index, 1) : null;
        }
    }

    const addEnemiesBullet = (enemiesArray) => {
        for (let enemy of enemiesArray) {
            if (enemy.name == "normal" || enemy.name == "worm") {
                setTimeout(function () {
                    !enemy.invisible ? enemiesBulletsArray.push(new EnemyBullet(enemy.x, enemy.y + (enemy.height / 2), settings)) : null;
                }, Math.random() * 1000);
            }
        }
        setTimeout(function () {
            addEnemiesBullet(enemiesArray);
        }, 4000);
    }

    const updateEnemiesBullets = () => {
        for (const [index, bullet] of enemiesBulletsArray.entries()) {
            bullet.updatePosition();
            bullet.x <= 0 ? enemiesBulletsArray.splice(index, 1) : null;
        }
    }
}