export default function Loading(images) {
    this.fileCounter = 0;
    this.endOfLoading = false;

    const displayAllFiles = () => {
        for (const img in images) {
            console.log(images[img]);
        }
    }

    //count all files to load
    (() => {
        for (let img in images) {
            this.fileCounter++;
        }
        console.log(`Amount of files: ${this.fileCounter}`);
    })();

    //add src to image (load files)
    (() => {
        for (let img in images) {
            images[img].src = `gfx/${img}.png`;
            images[img].onload = () => {
                this.fileCounter--;
                this.fileCounter == 0 ? this.endOfLoading = true : null;
                this.endOfLoading ? console.log(`Loaded all files`) : null;
                // this.endOfLoading ? displayAllFiles() : null;
            }
        }
    })();
}