require('./../scss/style.scss');

import Images from './Images.js';
import Loading from './Loading.js';
import Canvas from './Canvas.js';
import Generator from './Generator.js';
import Settings from './Settings.js';
import Player from "./Player";
import Ui from './Ui.js';

const settings = new Settings();
const images = new Images();
const loading = new Loading(images);
const player = new Player(settings);
const canvas = new Canvas(images, settings, player);
const generator = new Generator(canvas, player, settings, images);
const ui = new Ui(player);

const interval = setInterval(() => {
    if (loading.endOfLoading) {
        clearInterval(interval);
        generator.start();
    }
}, 100);