export default function Player(settings) {
    this.lives = 3;
    this.invisible = false;
    this.x = 50;
    this.y = 350;
    this.width = 80;
    this.height = 66;
    this.direction = null;
    this.image = 'player';
    this.score = 0;

    this.updatePlayerPosition = () => {
        if (this.invisible == false) {
            switch (this.direction) {
                case "left":
                    moveLeft()
                    break;
                case "right":
                    moveRight()
                    break;
                case "up":
                    moveUp()
                    break;
                case "down":
                    moveDown()
                    break;
            }
        }
    }

    this.dead = () => {
        if (this.lives >= 0) {
            this.lives--;
            settings.bulletsTime = 500;
            this.invisible = true;
            setTimeout(() => {
                this.image = 'dead0';
            }, 200);
            setTimeout(() => {
                this.image = 'dead1';
            }, 400);
            setTimeout(() => {
                this.image = 'dead2';
            }, 600);
            setTimeout(() => {
                this.image = 'dead3';
            }, 800);
            setTimeout(() => {
                this.image = 'dead4';
            }, 1000);
            setTimeout(() => {
                this.x = 50;
                this.y = 350;
                this.image = 'playerT';
                setTimeout(() => {
                    this.invisible = false;
                    this.image = 'player';
                }, 1000);
            }, 1200);
        }
        if (this.lives == 0) {
            setTimeout(function () {
                settings.start = "OVER";
            }, 1200);
        }
    }

    const moveUp = () => {
        this.y >= settings.playerSpeed ? this.y -= settings.playerSpeed : null;
    }

    const moveDown = () => {
        this.y <= (700 - this.height) ? this.y += settings.playerSpeed : null;
    }

    const moveRight = () => {
        this.x <= (800 - this.width - settings.playerSpeed) ? this.x += settings.playerSpeed : null;
    }

    const moveLeft = () => {
        this.x >= settings.playerSpeed ? this.x -= settings.playerSpeed : null;
    }
}