export default function PlayerBullet(x, y, settings) {
    this.x = x;
    this.y = y;
    this.width = 27;
    this.height = 27;

    this.updatePosition = () => {
        this.x += settings.bulletSpeed;
    }

    this.deleteBullet = () => {
        this.x = 1000;
    }
}