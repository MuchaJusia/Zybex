export default function Settings() {
    this.playfieldWidth = 24500;
    this.playerSpeed = 7;
    this.bulletsTime = 500;
    this.bulletSpeed = 14;
    this.start = true;
}