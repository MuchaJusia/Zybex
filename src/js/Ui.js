export default function Ui(player) {
    document.addEventListener("keydown", function (e) {
        switch (e.keyCode) {
            case 87:
                player.direction = "up";
                break;
            case 65:
                player.direction = "left";
                break;
            case 83:
                player.direction = "down";
                break;
            case 68:
                player.direction = "right";
                break;
        }
    });


    document.addEventListener("keyup", function (e) {
        e.keyCode === 87 && player.direction == "up" ? player.direction = null : null;
        e.keyCode === 65 && player.direction == "left" ? player.direction = null : null;
        e.keyCode === 83 && player.direction == "down" ? player.direction = null : null;
        e.keyCode === 68 && player.direction == "right" ? player.direction = null : null;
    });
}